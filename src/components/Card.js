import React from 'react';

const Card = (props) => {
    return (
        <div className="nk-card">
            <img src={props.imageSrc} alt={props.name} />
        </div>
    );
}

export default Card;