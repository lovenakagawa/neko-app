import React from 'react';
import logo from '../assets/logo.png';

const Header = () => {
    return (
        <header className="nk-header">
            <img src={logo} className="nk-logo" alt="cat app logo" />
        </header>
    );
}

export default Header;