import React from 'react';

const Button = (props) => {
    return (
        <button className="nk-button" onClick={props.onclick}>
            <i className="nk-icon nk-icon--loadmore"></i>Load More
        </button>
    );
}

export default Button;