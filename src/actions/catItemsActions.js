import store from '../store';
import axios from 'axios';

const url = 'https://api.thecatapi.com/v1/images/search?limit=10';
const headers = {
    'Content-Type': 'application/json',
    'X-Auth-Token': '67310375-8ccd-4d6f-8be3-e8bb3b117085',
};

export const fetchCatItems = () => (dispatch) => {
    const categoryId = store.getState().selectedCategoryId.selectedId || 1;

    axios.get(`${url}&order=ASC&category_ids=${categoryId}`, {headers})
        .then(res => {
            dispatch({type: "FETCH_CATITEMS_FULFILLED", payload: res.data})
        })
        .catch(err => {
            dispatch({type: "FETCH_CATITEMS_FAILED", payload: err})
        })
}

export const initialCatItems = () => (dispatch) => {
    const categoryId = store.getState().selectedCategoryId.selectedId || 1;

    axios.get(`${url}&category_ids=${categoryId}`, {headers})
        .then(res => {
            dispatch({type: "RESET_CATITEMS", payload: res.data})
        })
        .catch(err => {
            dispatch({type: "RESET_CATITEMS_FAILED", payload: err})
        })
}