import axios from 'axios';

const url = 'https://api.thecatapi.com/v1/categories';

export const fetchCategories = () => dispatch => {
    axios.get(url)
        .then(res => {
            dispatch({type: "FETCH_CATEGORIES_FULFILLED", payload: res.data})
        })
        .catch(err => {
            dispatch({type: "FETCH_CATEGORIES_FAILED", payload: err})
        })
}

export const getSelectedCategoryId = (id) => {
    return { type: "GET_SELECTED_CATEGORY_ID", payload: id }
}