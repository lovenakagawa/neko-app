import { combineReducers } from 'redux';

import { categoriesReducer, selectedCategoryIdReducer } from './categoriesReducer';
import catItemsReducer from './catItemsReducer';

const reducers = combineReducers({ 
    categories: categoriesReducer,
    selectedCategoryId: selectedCategoryIdReducer,
    cats: catItemsReducer
})

export default reducers;