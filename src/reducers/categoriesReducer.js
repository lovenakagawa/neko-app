const initialState = {
    fetching: false,
    items: [],
    selectedId: null,
    error: null
}

export const categoriesReducer = (state = initialState, action) => {
    switch(action.type) {
        case "FETCH_CATEGORIES_START": {
            return {...state, fetching: true}
        }
        case "FETCH_CATEGORIES_FAILED": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_CATEGORIES_FULFILLED": {
            return {...state, fetching: false, items: action.payload}
        }
        default: return state;
    }
}

export const selectedCategoryIdReducer = (state = initialState, action) => {
    switch(action.type) {
        case "GET_SELECTED_CATEGORY_ID": {
            return {...state, selectedId: action.payload}
        }
        default: return state;
    }
}