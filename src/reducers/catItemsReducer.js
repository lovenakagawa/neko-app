const initialState = {
    fetching: false,
    items: [],
    error: null
}

const catItemsReducer = (state = initialState, action) => {
    switch(action.type) {
        case "FETCH_CATITEMS_START": {
            return {...state, fetching: true}
        }
        case "FETCH_CATITEMS_FAILED": {
            return {...state, fetching: false, error: action.payload}
        }
        case "FETCH_CATITEMS_FULFILLED": {
            return {...state, fetching: false, items: [...state.items, ...action.payload]}
        }
        case "RESET_CATITEMS": {
            return {...state, fetching: true, items: action.payload}
        }
        default: return state;
    }
}

export default catItemsReducer;