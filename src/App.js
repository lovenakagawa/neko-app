import React, { Component } from 'react';
import { Layout } from './containers';

class App extends Component {
  render () {
    return (
      <div className="nk-app">
        <Layout />
      </div>
    );
  }
}

export default App;