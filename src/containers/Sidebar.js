import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchCategories, getSelectedCategoryId } from '../actions/categoriesActions';
import { initialCatItems } from '../actions/catItemsActions';

const mapStateToProps = state => ({
    categories: state.categories.items,
    selectedCategoryId: state.selectedCategoryId.selectedId
});

const mapDispatchToProps = {
    fetchCategories,
    getSelectedCategoryId,
    initialCatItems
};

class Sidebar extends Component {

    handleClick = (e) => {
        const self = e.target;
        const categoryId = self.dataset.id;
        const { getSelectedCategoryId, initialCatItems } = this.props;

        self.className = "selected";
        getSelectedCategoryId(categoryId);
        initialCatItems();
    }

    handleBlur = (e) => {
        const self = e.target;
        self.className = "";
    }

    renderCategoryList = () => {
        const { categories } = this.props;

        return (
            <ul className="nk-clickable-list">
                { categories.map( (category, i) => {
                    return (
                        <li key={i}>
                            <button
                                data-id={category.id}
                                onBlur={this.handleBlur}
                                onMouseDown={this.handleClick}>
                                {category.name}
                            </button>
                        </li>
                    )
                })}
            </ul>
        )
    }

    componentWillMount() {
        this.props.fetchCategories();
    }

    render() {
        return (
            <aside className="nk-sidebar">
                <h1>Categories</h1>
                {this.renderCategoryList()}
            </aside>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);