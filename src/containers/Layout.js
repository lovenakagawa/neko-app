import React, { Component } from 'react';
import { Header } from '../components';
import { Sidebar, Grid } from './';
import '../css/app.css';

class Layout extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />

                <main className="nk-layout">
                    <Sidebar />
                    <Grid />
                </main>
            </React.Fragment>
        );
    }
}

export default Layout;


