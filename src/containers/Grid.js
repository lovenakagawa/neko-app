import React, { Component } from 'react';
import { Button, Card } from '../components';
import { connect } from 'react-redux';
import { fetchCatItems } from '../actions/catItemsActions';

const mapStateToProps = state => ({
    cats: state.cats.items,
    selectedCategoryId: state.selectedCategoryId.selectedId
});

const mapDispatchToProps = {
    fetchCatItems
};

class Grid extends Component {
    renderCards = (props) => {
        const { cats } = this.props;

        return (
            <React.Fragment>
                {cats.map((cat, i) => {
                    return (
                        <div key={i} className="nk-grid__item">
                            <Card
                                imageSrc={cat.url}
                                name={cat.categories[0].name}
                            />
                        </div>
                    )
                })}
            </React.Fragment>
        );
    }

    componentWillMount() {
        this.props.fetchCatItems();
    }

    render() {
        const { fetchCatItems } = this.props;

        return (
            <article className="nk-grid">
                {this.renderCards()}
                <div className="nk-grid__item nk-grid__item--full-block">
                    <Button onclick={fetchCatItems}/>
                </div>
            </article>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Grid);